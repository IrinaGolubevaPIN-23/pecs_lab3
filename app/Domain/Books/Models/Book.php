<?php

namespace App\Domain\Books\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Database\Factories\BookFactory;

class Book extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'cost', 'writer_id'];
    public function author(): HasOne
    {
        return $this->hasOne(Writer::class);
    }
    public static function factory(): BookFactory
    {
        return BookFactory::new();
    }
}
