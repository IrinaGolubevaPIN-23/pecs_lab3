<?php

namespace App\Domain\Books\Actions;

use App\Domain\Books\Models\Book;

class CreateBookAction
{
    public function execute(array $fields): Book
    {
        $book = new Book();
        $book->name = $fields['name'];
        $book->cost = $fields['cost'];
        $book->writer_id=$fields['writer_id'];
        $book->save();
        return $book;
    }
}
