<?php

namespace App\Domain\Books\Actions;

use App\Domain\Books\Models\Book;

class PutBookAction
{
    public function execute(int $id, array $fields): Book
    {
        $book = Book::query()->findOrFail($id);
        $book->update($fields);
        $book->save();
        return $book;
    }
}
