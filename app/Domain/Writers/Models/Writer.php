<?php

namespace App\Domain\Writers\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Database\Factories\WriterFactory;

class Writer extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'second_name'];
    protected $primaryKey = 'id';
    public function books(): HasMany
    {
        return $this->hasMany(Book::class);
    }
    public static function factory(): WriterFactory
    {
        return WriterFactory::new();
    }
}
