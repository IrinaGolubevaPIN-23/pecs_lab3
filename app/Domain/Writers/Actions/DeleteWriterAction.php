<?php

namespace App\Domain\Writers\Actions;

use App\Domain\Writers\Models\Writer;

class DeleteWriterAction
{
    public function execute(int $id): void
    {
        Writer::where('id', $id)->delete();
    }
}

