<?php

namespace App\Domain\Writers\Actions;

use App\Domain\Writers\Models\Writer;

class PutWriterAction
{
    public function execute(int $id, array $fields): Writer
    {
        $writer = Writer::query()->findOrFail($id);
        $writer->update($fields);
        $writer->save();
        return $writer;
    }
}
