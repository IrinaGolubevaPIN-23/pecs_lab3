<?php

namespace App\Domain\Writers\Actions;

use App\Domain\Writers\Models\Writer;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class CreateWriterAction
{
    public function execute(array $fields): Writer
    {
        $writer = new Writer();
        $writer->name = $fields['name'];
        $writer->second_name = $fields['second_name'];
        $writer->save();
        return $writer;
    }
}
