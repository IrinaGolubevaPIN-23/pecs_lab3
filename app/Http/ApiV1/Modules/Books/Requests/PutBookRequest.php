<?php

namespace App\Http\ApiV1\Modules\Books\Requests;
use Illuminate\Validation\Rule;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Domain\Writers\Models\Writer;

class PutBookRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');
        return [
            'id' => [Rule::unique('books')->ignore($id)],
            'name' => ['required', 'string'],
            'cost' => ['required', 'integer'],
            'writer_id' => ['required', Rule::exists(Writer::class, 'id')],
        ];
    }
}
