<?php

namespace App\Http\ApiV1\Modules\Books\Requests;

use App\Domain\Books\Models\Book;
use App\Domain\Writers\Models\Writer;
use Illuminate\Validation\Rule;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateBookRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'id' => [[Rule::unique('books')],],
            'name' => ['required', 'string'],
            'cost' => ['required', 'integer'],
            'writer_id' => ['required', Rule::exists(Writer::class, 'id')],
        ];
    }
}
