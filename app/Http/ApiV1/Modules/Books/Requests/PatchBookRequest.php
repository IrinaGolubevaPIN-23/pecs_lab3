<?php

namespace App\Http\ApiV1\Modules\Books\Requests;
use Illuminate\Validation\Rule;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Domain\Writers\Models\Writer;

class PatchBookRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');
        return [
            'id' => [Rule::unique('books')->ignore($id)],
            'name' => ['nullable', 'string'],
            'cost' => ['nullable', 'integer'],
            'writer_id' => ['nullable', Rule::exists(Writer::class, 'id')],
        ];
    }
}
