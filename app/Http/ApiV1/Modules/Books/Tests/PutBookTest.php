<?php

use App\Domain\Books\Models\Book;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use function Pest\Laravel\assertDatabaseHas;
use Tests\TestCase;
use function Pest\Laravel\putJson;

uses(TestCase::class);
uses(DatabaseTransactions::class);
uses()->group('book');

test('PUT /api/v1/books/{id} Success', function () {
    $book = Book::factory()->create();
    putJson('/api/v1/books/'.$book->id, [
        'name'=>'The Game', 
        'cost'=>56400,
        'writer_id'=>4
    ])
    ->assertStatus(200)
    ->assertJsonPath('data.id', $book->id);
    assertDatabaseHas((new Book())->getTable(), [
        'name'=>'The Game', 
        'cost'=>56400,
        'writer_id'=>4
    ]);
    
});

test('PUT /api/v1/books/{id} Not Found', function () {
    putJson('/api/v1/books/'.-1,[
        'name'=>'The Game', 
        'cost'=>56400,
        'writer_id'=>4
    ])
        ->assertStatus(404);
});

test('PUT /api/v1/books/{id} Unprocessable Content', function () {
    $book = Book::factory()->create();
    putJson('/api/v1/books/'.$book->id, [
        'name'=>'The Game', 
    ])->assertStatus(422);
    
});

