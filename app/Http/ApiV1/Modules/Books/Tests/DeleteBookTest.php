<?php

use App\Domain\Books\Models\Book;

use Tests\TestCase;
use function Pest\Laravel\deleteJson;

uses(TestCase::class);
uses()->group('book');

test('DELETE /api/v1/books/{id} Success', function () {
    $book = Book::factory()->create();
    deleteJson('/api/v1/books/'.$book->id)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
