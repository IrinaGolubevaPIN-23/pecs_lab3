<?php

use App\Domain\Books\Models\Book;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use function Pest\Laravel\assertDatabaseHas;
use Tests\TestCase;
use function Pest\Laravel\postJson;

uses(TestCase::class);
uses(DatabaseTransactions::class);
uses()->group('book');

test('POST /api/v1/books Created', function () {
    postJson('/api/v1/books', [
        'name'=>'The Game', 
        'cost'=>56400,
        'writer_id'=>4
    ])->assertStatus(201);
    assertDatabaseHas((new Book())->getTable(), [
        'name'=>'The Game', 
        'cost'=>56400,
        'writer_id'=>4
    ]);
    
});

test('POST /api/v1/books Unprocessable Content', function () {
    postJson('/api/v1/books', [
        'name'=>'The Game'
    ])->assertStatus(422);
    
});
