<?php

use App\Domain\Books\Models\Book;

use Tests\TestCase;
use function Pest\Laravel\getJson;

uses(TestCase::class);
uses()->group('book');

test('GET /api/v1/books/{id} Success', function () {
    $book = Book::factory()->create();
    getJson('/api/v1/books/'.$book->id)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $book->id);
});

test('GET /api/v1/books/{id} Not Found', function () {
    getJson('/api/v1/books/'.-1)
        ->assertStatus(404);
});
