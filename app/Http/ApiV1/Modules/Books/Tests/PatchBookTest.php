<?php

use App\Domain\Books\Models\Book;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use function Pest\Laravel\assertDatabaseHas;
use Tests\TestCase;
use function Pest\Laravel\patchJson;

uses(TestCase::class);
uses(DatabaseTransactions::class);
uses()->group('book');

test('PATCH /api/v1/books/{id} Success', function ($field, $value) {
    $book = Book::factory()->create();
    patchJson('/api/v1/books/'.$book->id, [
        $field=>$value
    ])
    ->assertStatus(200)
    ->assertJsonPath('data.id', $book->id);
    assertDatabaseHas((new Book())->getTable(), [
        $field=>$value, 
    ]);
    
})->with([
    ['name', 'Bob'],
    ['cost', 123000],
    ['writer_id', 3]
]);

test('PATCH /api/v1/books/{id} Not Found', function ($field, $value) {
    patchJson('/api/v1/books/'.-1,[
        $field=>$value
    ])
    ->assertStatus(404);
})->with([
    ['name', 'Bob'],
    ['cost', 123000],
    ['writer_id', 3]
]); 
