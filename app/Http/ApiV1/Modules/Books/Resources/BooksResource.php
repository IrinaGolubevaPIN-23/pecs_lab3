<?php

namespace App\Http\ApiV1\Modules\Books\Resources;

use App\Domain\Books\Models\Book;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class BooksResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cost' => $this->cost,
            'writer_id' => $this->writer_id,
        ];
    }
}
