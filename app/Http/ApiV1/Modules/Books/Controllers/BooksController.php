<?php

namespace App\Http\ApiV1\Modules\Books\Controllers;

use App\Http\ApiV1\Modules\Books\Queries\BooksQuery;
use App\Http\ApiV1\Modules\Books\Resources\BooksResource;

use App\Http\ApiV1\Modules\Books\Requests\CreateBookRequest;
use App\Domain\Books\Actions\CreateBookAction;

use App\Http\ApiV1\Modules\Books\Requests\PutBookRequest;
use App\Domain\Books\Actions\PutBookAction;

use App\Http\ApiV1\Modules\Books\Requests\PatchBookRequest;
use App\Domain\Books\Actions\PatchBookAction;

use App\Domain\Books\Actions\DeleteBookAction;

use App\Http\ApiV1\Support\Resources\EmptyResource;


class BooksController
{
    public function get(int $id, BooksQuery $query): BooksResource
    {
        return new BooksResource($query->findOrFail($id));
    }
    public function create(CreateBookRequest $request, CreateBookAction $action): BooksResource
    {
        return new BooksResource($action->execute($request->validated()));
    }
    public function put(int $id, PutBookRequest $request, PutBookAction $action): BooksResource
    {
        return new BooksResource($action->execute($id,$request->validated()));
    }

    public function patch(int $id, PatchBookRequest $request, PatchBookAction $action): BooksResource
    {
        return new BooksResource($action->execute($id,$request->validated()));
    }

    public function delete(int $id, DeleteBookAction $action): EmptyResource
    {
        $action->execute($id);
        return new EmptyResource();
    }

}
