<?php
namespace App\Http\ApiV1\Modules\Writers\Resources;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

use App\Domain\Writers\Models\Writer;

/** @mixin \App\Domain\Users\Models\User */
class WritersResource extends BaseJsonResource
{
public function toArray($request): array
{
    return [
        'id' => $this->id,
        'name' => $this->name,
        'second_name' => $this->second_name,
    ];
    }
}
