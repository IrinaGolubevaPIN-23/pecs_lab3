<?php

namespace App\Http\ApiV1\Modules\Writers\Queries;

use App\Domain\Writers\Models\Writer;
use Spatie\QueryBuilder\QueryBuilder;

class WritersQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Writer::query();

        parent::__construct($query);

        $this->defaultSort('id');
    }
}
