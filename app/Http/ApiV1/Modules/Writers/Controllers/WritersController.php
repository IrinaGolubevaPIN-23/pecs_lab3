<?php


namespace App\Http\ApiV1\Modules\Writers\Controllers;
use App\Http\ApiV1\Modules\Writers\Queries\WritersQuery;
use App\Http\ApiV1\Modules\Writers\Resources\WritersResource;

use App\Http\ApiV1\Modules\Writers\Requests\CreateWriterRequest;
use App\Domain\Writers\Actions\CreateWriterAction;

use App\Http\ApiV1\Modules\Writers\Requests\PutWriterRequest;
use App\Domain\Writers\Actions\PutWriterAction;

use App\Http\ApiV1\Modules\Writers\Requests\PatchWriterRequest;
use App\Domain\Writers\Actions\PatchWriterAction;

use App\Domain\Writers\Actions\DeleteWriterAction;

use App\Http\ApiV1\Support\Resources\EmptyResource;


class WritersController
{
    public function get(int $id, WritersQuery $query):WritersResource
    {
        return new WritersResource($query->findOrFail($id));
    }

    public function create(CreateWriterRequest $request, CreateWriterAction $action):WritersResource
    {
        return new WritersResource($action->execute($request->validated()));
    }

    public function put(int $id, PutWriterRequest $request, PutWriterAction $action):WritersResource
    {
        return new WritersResource($action->execute($id,$request->validated()));
    }

    public function patch(int $id, PatchWriterRequest $request, PatchWriterAction $action):WritersResource
    {
        return new WritersResource($action->execute($id,$request->validated()));
    }

    public function delete(int $id, DeleteWriterAction $action):EmptyResource
    {
        $action->execute($id);
        return new EmptyResource();
    }
}
