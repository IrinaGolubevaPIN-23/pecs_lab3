<?php

use App\Domain\Writers\Models\Writer;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use function Pest\Laravel\assertDatabaseHas;
use Tests\TestCase;
use function Pest\Laravel\patchJson;

uses(TestCase::class);
uses(DatabaseTransactions::class);
uses()->group('writer');

test('PATCH /api/v1/writers/{id} Success', function ($field, $value) {
    $writer = Writer::factory()->create();
    patchJson('/api/v1/writers/'.$writer->id, [
        $field=>$value
    ])
    ->assertStatus(200)
    ->assertJsonPath('data.id', $writer->id);
    assertDatabaseHas((new Writer())->getTable(), [
        $field=>$value, 
    ]);
    
})->with([
    ['name', 'Bob'],
    ['second_name', 'Parse']
]);

test('PATCH /api/v1/writers/{id} Not Found', function ($field, $value) {
    patchJson('/api/v1/writers/'.-1,[
        $field=>$value
    ])
    ->assertStatus(404);
})->with([
    ['name', 'Bob'],
    ['second_name', 'Parse']
]);
