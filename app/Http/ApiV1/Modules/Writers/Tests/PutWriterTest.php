<?php

use App\Domain\Writers\Models\Writer;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use function Pest\Laravel\assertDatabaseHas;
use Tests\TestCase;
use function Pest\Laravel\putJson;

uses(TestCase::class);
uses(DatabaseTransactions::class);
uses()->group('writer');

test('PUT /api/v1/writers/{id} Success', function () {
    $writer = Writer::factory()->create();
    putJson('/api/v1/writers/'.$writer->id, [
        'name'=>'Bob', 
        'second_name'=>'Parse'
    ])
    ->assertStatus(200)
    ->assertJsonPath('data.id', $writer->id);
    assertDatabaseHas((new Writer())->getTable(), [
        'name' => 'Bob',
        'second_name' => 'Parse'
    ]);
    
});

test('PUT /api/v1/writers/{id} Not Found', function () {
    putJson('/api/v1/writers/'.-1,[
        'name'=>'Bob', 
        'second_name'=>'Parse'
    ])
        ->assertStatus(404);
});

test('PUT /api/v1/writers/{id} Unprocessable Content', function () {
    $writer = Writer::factory()->create();
    putJson('/api/v1/writers/'.$writer->id, [
        'name'=>'Bob'
    ])->assertStatus(422);
    
});

