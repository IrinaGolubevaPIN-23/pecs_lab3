<?php

use App\Domain\Writers\Models\Writer;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use function Pest\Laravel\assertDatabaseHas;
use Tests\TestCase;
use function Pest\Laravel\postJson;

uses(TestCase::class);
uses(DatabaseTransactions::class);
uses()->group('writer');

test('POST /api/v1/writers Created', function () {
    postJson('/api/v1/writers', [
        'name'=>'Bob', 
        'second_name'=>'Parse'
    ])->assertStatus(201);
    assertDatabaseHas((new Writer())->getTable(), [
        'name' => 'Bob',
        'second_name' => 'Parse'
    ]);
    
});

test('POST /api/v1/writers Unprocessable Content', function () {
    postJson('/api/v1/writers', [
        'name'=>'Bob'
    ])->assertStatus(422);
    
});
