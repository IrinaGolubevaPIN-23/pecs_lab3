<?php

use App\Domain\Writers\Models\Writer;

use Tests\TestCase;
use function Pest\Laravel\deleteJson;

uses(TestCase::class);
uses()->group('writer');

test('DELETE /api/v1/writers/{id} Success', function () {
    $writer = Writer::factory()->create();
    deleteJson('/api/v1/writers/'.$writer->id)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
