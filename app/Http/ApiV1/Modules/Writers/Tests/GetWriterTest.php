<?php

use App\Domain\Writers\Models\Writer;

use Tests\TestCase;
use function Pest\Laravel\getJson;

uses(TestCase::class);
uses()->group('writer');

test('GET /api/v1/writers/{id} Success', function () {
    $writer = Writer::factory()->create();
    getJson('/api/v1/writers/'.$writer->id)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $writer->id);
});

test('GET /api/v1/writers/{id} Not Found', function () {
    getJson('/api/v1/writers/'.-1)
        ->assertStatus(404);
});
