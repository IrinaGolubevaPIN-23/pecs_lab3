<?php

namespace App\Http\ApiV1\Modules\Writers\Requests;
use Illuminate\Validation\Rule;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateWriterRequest extends BaseFormRequest
{
    public function rules(): array
    {
       
        return [
            'id' => [Rule::unique('writers')],
            'name' =>['required', 'string'],
            'second_name' => ['required', 'string']
        ];
    }
}
