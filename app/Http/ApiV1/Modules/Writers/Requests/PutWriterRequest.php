<?php

namespace App\Http\ApiV1\Modules\Writers\Requests;
use Illuminate\Validation\Rule;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PutWriterRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');
        return [
            'id' => [Rule::unique('writers')->ignore($id)],
            'name' =>['required', 'string'],
            'second_name' => ['required', 'string']
        ];
    }
}
