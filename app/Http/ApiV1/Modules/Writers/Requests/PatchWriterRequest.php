<?php

namespace App\Http\ApiV1\Modules\Writers\Requests;
use Illuminate\Validation\Rule;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchWriterRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');
        return [
            'id' => [Rule::unique('writers')->ignore($id)],
            'name' => ['nullable','string'],
            'second_name' => ['nullable','string']
        ];
    }
}
