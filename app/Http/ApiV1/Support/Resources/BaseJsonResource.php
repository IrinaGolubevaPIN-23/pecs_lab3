<?php

namespace App\Http\ApiV1\Support\Resources;

use DateTime;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

abstract class BaseJsonResource extends JsonResource
{
    public const DATE_FORMAT = 'Y-m-d';

    public function dateToIso(?DateTime $date): ?string
    {
        return $date ? $date->format(static::DATE_FORMAT) : null;
    }
}
