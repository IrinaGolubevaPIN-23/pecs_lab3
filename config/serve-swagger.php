<?php

return [
    'path' => 'docs/swagger',

    'urls' => [
        [
            'url' => 'index.yaml', 
            'name' => 'Documentation' 
        ],
    ],
];
