<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Domain\Writers\Models\Writer;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Writer>
 */
class WriterFactory extends Factory
{
    protected $model = Writer::class;
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'second_name' => $this->faker->word,
        ];
    }
}
