<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Domain\Books\Models\Book;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    protected $model = Book::class;
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'cost' => $this->faker->numberBetween(1000, 20000),
            'writer_id' => $this->faker->numberBetween(0, 10)
        ];
    }
}
