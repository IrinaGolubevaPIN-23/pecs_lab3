<?php

namespace Database\Seeders;
use App\Domain\Writers\Models\Writer;
use App\Domain\Books\Models\Book;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Writer::factory()
            ->count(10)
            ->create();
        Book::factory()
            ->count(10)
            ->create();
    }
}
