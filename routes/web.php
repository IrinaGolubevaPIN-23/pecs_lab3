<?php

use Illuminate\Support\Facades\Route;
use App\Http\ApiV1\Modules\Writers\Controllers\WritersController;
use App\Http\ApiV1\Modules\Books\Controllers\BooksController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/v1/writers/{id}', [WritersController::class,'get']);
Route::post('/api/v1/writers', [WritersController::class,'create']);
Route::put('/api/v1/writers/{id}', [WritersController::class,'put']);
Route::patch('/api/v1/writers/{id}', [WritersController::class,'patch']);
Route::delete('/api/v1/writers/{id}', [WritersController::class,'delete']);


Route::get('/api/v1/books/{id}', [BooksController::class,'get']);
Route::post('/api/v1/books', [BooksController::class,'create']);
Route::put('/api/v1/books/{id}', [BooksController::class,'put']);
Route::patch('/api/v1/books/{id}', [BooksController::class,'patch']);
Route::delete('/api/v1/books/{id}', [BooksController::class,'delete']);
